<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 16:31
 */

namespace blog\data\Mapper;

use blog\data\Entity\Post;

class PostMapper {

    /**
     * @param array $data
     * @return array
     */
    public function getDomainData(array $data){

        $result = array();

        foreach($data as $d) {

            $p = new Post();
            $p->setId($d['id']);
            $p->setTitle($d['title']);
            $p->setContent($d['content']);
            $p->setPublished($d['published']);
            $result[]= $p;

        }

        return $result;
    }

} 