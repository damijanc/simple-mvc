<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 10:48
 */

namespace blog\data\Entity;


/**
 * Class Entity
 * @package blog\data\Entity
 */
class Entity {


    /**
     * @id
     */
    private $id;

    /**
     * @string
     */
    private $created;

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



} 