<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 10:49
 */

namespace blog\data\Entity;


class Comment extends Entity {

    private $post_id;
    private $content;
    private $email;
    private $url;

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param int $post_id
     */
    public function setPostId($post_id)
    {
        $this->post_id = $post_id;
    }

    /**
     * @return int
     */
    public function getPostId()
    {
        return $this->post_id;
    }




} 